import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField, {Input} from '@material/react-text-field';

export default class SearchBar extends Component {

  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleSearchChange(newValue) {
    this.setState({ value: newValue });
    this.props.onSearchChange(newValue);
  }

  render() {  
    return (
      <div className="SearchBar">
        <TextField label='Search'>
          <Input
            value={this.state.value}
            onChange={(e) => this.handleSearchChange(e.target.value)}/>
        </TextField>
      </div>
    );
  }
}
SearchBar.propTypes = {
  onSearchChange: PropTypes.func.isRequired,
};
