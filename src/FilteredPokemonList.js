import React from 'react';
import PropTypes from 'prop-types';
import Fuse from 'fuse.js';
import { POKEMON_PROP_TYPE } from './propTypes';
import PokemonList from './PokemonList';
import filterPokemon from './filterPokemon';

const FUSE_OPTIONS = {
  shouldSort: true,
  threshold: 0.35,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "name",
    "type"
  ]
}

/**
 * Applies a set of filters to the provided list of pokemon.
 */
function FilteredPokemonList(props) {
  const { pokemonList, typeFilter, maxHeightFilter, searchFilter } = props;
  const filteredPokemon = filterPokemon(pokemonList, typeFilter, maxHeightFilter);
  let searchedAndFilteredPokemon;
  if (searchFilter === '') {
    searchedAndFilteredPokemon = filteredPokemon;
  } else {
    // TODO(matt): See whether there is a more efficient way that avoids instantiating a new fuse instance each time.
    const fuseInstance = new Fuse(filteredPokemon, FUSE_OPTIONS);
    searchedAndFilteredPokemon = fuseInstance.search(searchFilter);
  }

  return <PokemonList pokemonList={searchedAndFilteredPokemon} />;
}
FilteredPokemonList.propTypes = {
  pokemonList: PropTypes.arrayOf(POKEMON_PROP_TYPE),
  typeFilter: PropTypes.string,
  maxHeightFilter: PropTypes.number,
  searchFilter: PropTypes.string,
};

export default FilteredPokemonList;
