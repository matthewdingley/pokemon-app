import React from 'react';
import { POKEMON_PROP_TYPE } from './propTypes';
import Card from "@material/react-card";
import './PokemonTile.scss';


/**
 * Renders a single Pokemon.
 */
export default function PokemonTile({ pokemon }) {
  const { name, img, type, height } = pokemon;

  return (
    <div className="PokemonTile">
      <Card>
        <div className="PokemonTileImage">
          <img src={img} alt={name} />
        </div>
        <h2>{name}</h2>
        <p>{type.join(', ')}</p>
        <p>{height}</p>
      </Card>
    </div>
  );
}
PokemonTile.propTypes = {
  pokemon: POKEMON_PROP_TYPE,
};