import React, { Component } from 'react';
import SearchBar from './SearchBar';
import FilteredPokemonList from './FilteredPokemonList';
import LoadingList from './LoadingList';
import TypeSelectionBar, { ALL_TYPES_VALUE } from './TypeSelectionBar';
import HeightSelectionMenu from './HeightSelectionMenu';
import pokemonListHeightBounds from './pokemonListHeightBounds';
import updatePokemonHeights from './updatePokemonHeights';
import './FilterableCatalog.scss';

const POKEMON_LIST_URI = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';

function LoadingFailed(props) {
  // TODO(matt): Display a nice message with a button to try again. Have the button emit a relevant event.
  return (<div>Loading failed</div>);
}

/**
 * Fetches the Pokemon list.
 */
function fetchPokemon() {
  return fetch(POKEMON_LIST_URI)
    .then((response) => {
      return response.json();
    })
    .then((jsonResponse) => {
      if ('pokemon' in jsonResponse) {
        return jsonResponse.pokemon;
      }
      throw new Error('Pokemon data file in incorrect format');
    });
}

class FilterableCatalog extends Component {
  constructor(props) {
    super(props);
    this.state ={
      pokemonList: [],
      pokemonLoaded: false,
      pokemonLoadingFailed: false,
      searchFilter: '',
      typeFilter: ALL_TYPES_VALUE,
      maxHeightFilter: undefined,
      minHeight: undefined,
      maxHeight: undefined,
    };
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleHeightChange = this.handleHeightChange.bind(this);
  }

  componentDidMount() {
    fetchPokemon()
      .then((pokemonList) => {
        const updatedPokemon = updatePokemonHeights(pokemonList);
        const { min, max } = pokemonListHeightBounds(updatedPokemon);
        this.setState({
          pokemonList: updatedPokemon,
          pokemonLoaded: true,
          minHeight: min,
          maxHeight: max
        });
      })
      .catch((err) => {
        // TODO(matt): Consider allowing the error to be logged somewhere.
        this.setState({ pokemonLoadingFailed: true });
      })
  }

  handleTypeChange(newType) {
    this.setState({ typeFilter: newType });
  }

  handleSearchChange(newSearch) {
    this.setState({ searchFilter: newSearch });
  }

  handleHeightChange(newHeight) {
    this.setState({ maxHeightFilter: newHeight });
  }

  render() {
    let list;
    if (this.state.pokemonLoadingFailed) {
      list = <LoadingFailed />;
    } else if (this.state.pokemonLoaded) {
      list = <FilteredPokemonList {...this.state} />;
    } else {
      list = <LoadingList />;
    }
    return (
      <div className="FilterableCatalog">
        <div className="FilterableCatalogFilters">
          <div className="FilterableCatalogFilter">
            <SearchBar searchFilter={this.state.searchFilter} onSearchChange={this.handleSearchChange} />
          </div>
          <div className="FilterableCatalogFilter">
            <TypeSelectionBar pokemonList={this.state.pokemonList} onTypeChange={this.handleTypeChange} />
          </div>
          <div className="FilterableCatalogFilter">
            <HeightSelectionMenu
              pokemonList={this.state.pokemonList}
              onHeightChange={this.handleHeightChange}
              minHeight={this.state.minHeight}
              maxHeight={this.state.maxHeight}
            />
          </div>
        </div>

        {list}
      </div>
    );
  }
}

export default FilterableCatalog;
