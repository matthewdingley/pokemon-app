import React from 'react';
import FilterableCatalog from './FilterableCatalog';
import './App.scss';

function App() {  
  return (
    <div className="App">
      <header className="App-header">
        <h1>The Pokemon Catalog</h1>
        <FilterableCatalog />
      </header>
    </div>
  );
}

export default App;
