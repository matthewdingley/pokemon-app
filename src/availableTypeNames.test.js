import { aPokemon } from './testing/PokemonBuilder';
import availableTypeNames from './availableTypeNames';

const TYPE_ONE = 'Type A';
const TYPE_TWO = 'Type B';
const TYPE_THREE = 'Type C';

it('returns the types for two simple pokemon', () => {
  const pokemonList = [
    aPokemon().withId(1).withType(TYPE_ONE).build(),
    aPokemon().withId(2).withType(TYPE_TWO).build(),
  ];
  const names = availableTypeNames(pokemonList).sort();
  expect(names).toEqual([TYPE_ONE, TYPE_TWO]);
});

it('returns the types for pokemon with multiple types each', () => {
  const pokemonList = [
    aPokemon().withId(1).withType(TYPE_ONE).withType(TYPE_TWO).build(),
    aPokemon().withId(2).withType(TYPE_TWO).withType(TYPE_THREE).build(),
  ];
  const names = availableTypeNames(pokemonList).sort();
  expect(names).toEqual([TYPE_ONE, TYPE_TWO, TYPE_THREE]);
});
