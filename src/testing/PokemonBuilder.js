const DEFAULT_POKEMON = {
  "num": "001",
  "img": "http://www.serebii.net/pokemongo/pokemon/001.png",
  "weight": "6.9 kg",
  "candy": "Bulbasaur Candy",
  "candy_count": 25,
  "egg": "2 km",
  "spawn_chance": 0.69,
  "avg_spawns": 69,
  "spawn_time": "20:00",
};

const DEFAULT_NAME = 'Bulbasaur';
const DEFAULT_TYPES = ['Grass', 'Poison'];
const DEFAULT_HEIGHT = '0.71 m';

/**
 * A test data builder for Pokemon objects.
 */
class PokemonBuilder {
  constructor() {
    this.id = 0;
    this.name = undefined;
    this.height = undefined;
    this.type = [];
  }

  withId(id) {
    this.id = id;
    return this;
  }

  withName(name) {
    this.name = name;
    return this;
  }

  withHeight(height) {
    this.height = height;
    return this;
  }

  withType(type) {
    this.type.push(type);
    return this;
  }

  build() {
    const name = (this.name === undefined) ? DEFAULT_NAME : this.name;
    const height = (this.height === undefined) ? DEFAULT_HEIGHT : this.height;
    const type = (this.type.length === 0) ? [...DEFAULT_TYPES] : this.type;

    return {
      ...DEFAULT_POKEMON,
      id: this.id,
      name,
      height,
      type
    };
  }
}

export function aPokemon() {
  return new PokemonBuilder();
}
