import React from 'react';
import PropTypes from 'prop-types';
import { POKEMON_PROP_TYPE } from './propTypes';
import PokemonTile from './PokemonTile';
import './PokemonList.scss';

/**
 * Renders the provided list of Pokemon.
 */
function PokemonList(props) {
  // TODO(matt): Switch to a virtualised grid list
  const { pokemonList } = props;
  const displayedPokemon = pokemonList.map(
    individual => <PokemonTile pokemon={individual} key={individual.id} />);

  return (
    <div className="PokemonList">
      <p>Total Pokemon: {pokemonList.length}</p>
      <div className="PokemonListItems">{displayedPokemon}</div>
    </div>
  );
}
PokemonList.propTypes = {
  pokemonList: PropTypes.arrayOf(POKEMON_PROP_TYPE),
};

export default PokemonList;
