/**
 * Pulls out the available pokemon types from a list of Pokemon objects.
 * @returns {Array.<String>} An array of the available type names.
 */
export default function availableTypeNames(pokemonList) {
  const typeSet = new Set();
  pokemonList.forEach((pokemon) => {
    const { type } = pokemon;
    type.forEach(type => typeSet.add(type));
  });
  return Array.from(typeSet);
}
