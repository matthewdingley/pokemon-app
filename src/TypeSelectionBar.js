import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from '@material/react-select';
import { POKEMON_PROP_TYPE } from './propTypes';
import availableTypeNames from './availableTypeNames';

// Note that the Select component requires all options to have a non-empty value property
// otherwise it shows a collapsed box by default.
export const ALL_TYPES_VALUE = '*all';

const ALL_TYPES_OPTION = { label: 'All Types', value: ALL_TYPES_VALUE };

export default class TypeSelectionBar extends Component {
  constructor(props) {
    super(props);
    this.state = { value: ALL_TYPES_VALUE };
    this.handleTypeChange = this.handleTypeChange.bind(this);
  }

  handleTypeChange(newValue) {
    this.setState({ value: newValue });
    this.props.onTypeChange(newValue);
  }

  render() {
    const availableTypeOptions = availableTypeNames(this.props.pokemonList)
      .sort()
      .map(name => ({ label: name, value: name }));

    const allTypeOptions = [{ ...ALL_TYPES_OPTION }, ...availableTypeOptions];
    return (
      <Select
          label='Pokemon Type'
          onChange={(evt) => this.handleTypeChange(evt.target.value )}
          options={allTypeOptions}
          value={this.state.value}
        />
    );
  }
}
TypeSelectionBar.propTypes = {
  onTypeChange: PropTypes.func.isRequired,
  pokemonList: PropTypes.arrayOf(POKEMON_PROP_TYPE).isRequired,
};

