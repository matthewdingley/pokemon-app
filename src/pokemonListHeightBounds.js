/**
 * Returns the minimum and maximum heights found in a list of pokemon.
 * @param {Array.<Object>} pokemonList The non-empty list of pokemon.
 */
export default function pokemonListHeightBounds(pokemonList) {
  let minFound = Number.POSITIVE_INFINITY;
  let maxFound = 0;
  pokemonList.forEach((pokemon) => {
    const { heightNum } = pokemon;
    if (heightNum < minFound) {
      minFound = heightNum;
    }
    if (heightNum > maxFound) {
      maxFound = heightNum;
    }
  });
  return {min: minFound, max: maxFound };
}
