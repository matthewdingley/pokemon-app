const HEIGHT_REGEXP = /^(\d+.\d+) m$/;

/**
 * Produces an updated list of pokemon objects to include a numerical height.
 */
export default function updatePokemonHeights(pokemonList) {
  // TODO(matt): Consider avoiding creating a fresh set of objects here
  return pokemonList.map((pokemon) => {
    const matches = pokemon.height.match(HEIGHT_REGEXP);
    if (matches === null) {
      throw new Error(`Invalid height received: ${pokemon.height} for Pokemon ${pokemon.id}`);
    }
    const heightNum = parseFloat(matches[0]);
    return {
      ...pokemon,
      heightNum,
    };
  })
}
