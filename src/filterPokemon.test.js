import { aPokemon } from './testing/PokemonBuilder';
import { ALL_TYPES_VALUE } from './TypeSelectionBar';
import updatePokemonHeights from './updatePokemonHeights';
import filterPokemon from './filterPokemon';

const TYPE_ONE = 'Type One';
const TYPE_TWO = 'Type Two';
const TYPE_THREE = 'Type Three';

const SMALL_POKEMON = aPokemon().withId(1).withHeight('0.2 m').withType(TYPE_ONE);
const MEDIUM_POKEMON = aPokemon().withId(2).withHeight('0.5 m').withType(TYPE_ONE);
const TALL_POKEMON = aPokemon().withId(3).withHeight('1.2 m').withType(TYPE_ONE);

const MULTI_TYPE_POKEMON = aPokemon()
  .withId(4)
  .withHeight('0.6 m')
  .withType(TYPE_TWO)
  .withType(TYPE_THREE);

function sortedPokemonIds(pokemonList) {
  return pokemonList.map(pokemon => pokemon.id).sort();
}

describe('when filtering by height only', () => {
  let pokemonList;

  beforeEach(() => {
    pokemonList = updatePokemonHeights([
      SMALL_POKEMON.build(),
      MEDIUM_POKEMON.build(),
      TALL_POKEMON.build()
    ]);
  });

  it('lists all pokemon when filtering by a number that\'s larger than the tallest', () => {
    const filteredPokemon = filterPokemon(pokemonList, ALL_TYPES_VALUE, 1.6);
    expect(sortedPokemonIds(filteredPokemon)).toEqual([1, 2, 3]);
  });

  it('lists correct pokemon with a mid-range number', () => {
    const filteredPokemon = filterPokemon(pokemonList, ALL_TYPES_VALUE, 0.7);
    expect(sortedPokemonIds(filteredPokemon)).toEqual([1, 2]);
  });

  it('lists correct pokemon with a number that matches a pokemon height', () => {
    const filteredPokemon = filterPokemon(pokemonList, ALL_TYPES_VALUE, 0.5);
    expect(sortedPokemonIds(filteredPokemon)).toEqual([1, 2]);
  });

  it('lists zero pokemon with a low number', () => {
    const filteredPokemon = filterPokemon(pokemonList, ALL_TYPES_VALUE, 0.1);
    expect(filteredPokemon).toHaveLength(0);
  });
});

describe('when filtering by type only', () => {
  let pokemonList;

  beforeEach(() => {
    pokemonList = updatePokemonHeights([
      SMALL_POKEMON.build(),
      MULTI_TYPE_POKEMON.build(),
    ]);
  });

  it('returns the correct pokemon with one type', () => {
    const filteredPokemon = filterPokemon(pokemonList, TYPE_ONE, undefined);
    expect(filteredPokemon).toHaveLength(1);
    expect(filteredPokemon[0].id).toEqual(1);
  });

  it('returns the correct pokemon with multiple types', () => {
    const filteredPokemon = filterPokemon(pokemonList, TYPE_THREE, undefined);
    expect(filteredPokemon).toHaveLength(1);
    expect(filteredPokemon[0].id).toEqual(4);
  });
});

describe('when filtering by both type and height', () => {
  let pokemonList;

  beforeEach(() => {
    pokemonList = updatePokemonHeights([
      SMALL_POKEMON.build(),
      MEDIUM_POKEMON.build(),
      TALL_POKEMON.build(),
      MULTI_TYPE_POKEMON.build()
    ]);
  });

  it('returns the correct matching pokemon', () => {
    const filteredPokemon = filterPokemon(pokemonList, TYPE_ONE, 0.6);
    expect(sortedPokemonIds(filteredPokemon)).toEqual([1, 2]);
  });

  it('returns an empty list when no pokemon should match', () => {
    const filteredPokemon = filterPokemon(pokemonList, TYPE_TWO, 0.55);
    expect(filteredPokemon).toHaveLength(0);
  });
});
