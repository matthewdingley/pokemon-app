import { ALL_TYPES_VALUE } from './TypeSelectionBar';

/**
 * Filters a pokemon list according to a set of basic properties.
 * Search text filtering is done in a different step.
 * @param {Array.<Object>} pokemonList The list of Pokemon.
 * @param {String} typeFilter The type of pokemon to show or the ALL_TYPES_VALUE.
 * @param {Number} heightFilter The maximum height of Pokemon to show.
 */
export default function filterPokemon(pokemonList, typeFilter, maxHeightFilter) {
  const elementTest = (pokemon) => {
    if (maxHeightFilter !== undefined && pokemon.heightNum > maxHeightFilter) {
      return false;
    }
    if (typeFilter === ALL_TYPES_VALUE) {
      return true;
    } else if (pokemon.type.includes(typeFilter)) {
      return true;
    }
    return false;
  };
  return pokemonList.filter(elementTest);
}