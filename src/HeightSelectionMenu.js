import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MenuSurface, {Corner} from '@material/react-menu-surface';
import Button from '@material/react-button';
import { POKEMON_PROP_TYPE } from './propTypes';

// TODO(matt): Calculate this dynamically based on the input heights
const STEP_VALUE = 0.2;
const MAX_COMPARISON_EPSILON = 0.001;

// TODO(matt): This class could do with refactoring - too many responsibilities
export default class HeightSelectionMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      anchorElement: null,
      value: undefined,
    };
    this.setAnchorElement = this.setAnchorElement.bind(this);
    this.handleHeightChange = this.handleHeightChange.bind(this);
  }

  setAnchorElement(element) {
    if (this.state.anchorElement) {
      return;
    }
    this.setState({anchorElement: element});
  }

  handleHeightChange(newHeight) {
    const floatHeight = parseFloat(newHeight);
    this.setState({ value: floatHeight });
    this.props.onHeightChange(floatHeight);
  }

  render() {
    const { pokemonList, minHeight, maxHeight } = this.props;
    const { value, open, anchorElement } = this.state;
    // The max value is the next whole step value after the heighest size.
    // This means the next step down should start immediately removing items.
    let menuContent = null;
    if (open) {
      let updatedIntValue = (value === undefined) ? maxHeight : value;
      const maxValue = Math.ceil(maxHeight / STEP_VALUE) * STEP_VALUE;
      if (value === undefined) {
        updatedIntValue = maxValue;
      }
      const isAtMaxPosition = Math.abs(maxValue - updatedIntValue) < MAX_COMPARISON_EPSILON;
      let outputValue = (isAtMaxPosition) ? 'No limit' : updatedIntValue;
      menuContent = (
          <div>
            <p>Maximum height:</p>
            <input
              type="range"
              min={minHeight}
              max={maxValue}
              value={updatedIntValue}
              step={STEP_VALUE}
              onChange={(evt) => this.handleHeightChange(evt.target.value )} />
            {outputValue}
          </div>
      );
    }
    const buttonIsDisabled = pokemonList.length === 0;
    return (
      <div
        className='mdc-menu-surface--anchor'
        ref={this.setAnchorElement}
      >
        <Button

          disabled={buttonIsDisabled}
          onClick={() => this.setState({ open: true })}>Height</Button>
        <MenuSurface
          open={open}
          anchorCorner={Corner.BOTTOM_LEFT}
          onClose={() => this.setState({ open: false })}
          anchorElement={anchorElement}
        >
          {menuContent}
        </MenuSurface>
      </div>
    );
  }
}
HeightSelectionMenu.propTypes = {
  onHeightChange: PropTypes.func.isRequired,
  pokemonList: PropTypes.arrayOf(POKEMON_PROP_TYPE).isRequired,
  minHeight: PropTypes.number,
  maxHeight: PropTypes.number,
};
