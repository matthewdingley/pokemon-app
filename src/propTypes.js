import PropTypes from 'prop-types';

export const POKEMON_PROP_TYPE = PropTypes.shape({
    id: PropTypes.number.isRequired,
    num: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    type: PropTypes.arrayOf(PropTypes.string).isRequired,
    height: PropTypes.string.isRequired
  });
